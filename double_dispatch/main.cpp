#include <iostream>
#include <string>

const char * drawSentence = " drawn on ";

class IShape {
public:
    virtual std::string name() = 0;
};


class IDisplay {
public:
    virtual std::string name() const = 0;
};

class Circle : public IShape {
public:
    std::string name() const {
        return "Circle";
    }
};

class Square : public IShape {
public:
    std::string name() const {
        return "Square";
    }
};

class Screen : public IDisplay {
public:
    std::string name() const {
        return "Screen";
    }
};

class Projector : public IDisplay {
public:
    std::string name() const {
        return "Projector";
    }
};

int main()
{
    IShape * shape = new Circle();
    IDisplay * display = new Projector();

    std::string outcome = display->Draw(shape);
    if (outcome == shape->name() + drawSentence + display->name())
        std::cout << "PASSED";
    else
        std::cout << "FAILED";

    delete shape;
    delete display;

    shape = new Square();
    display = new Screen();

    outcome = display->Draw(shape);
    if (outcome == shape->name() + drawSentence + display->name())
        std::cout << "PASSED";
    else
        std::cout << "FAILED";

    delete shape;
    delete display;

    return 0;
}
